#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
#rc('font',**{'family':'serif','serif':['Computer Modern']})
#rc('text', usetex=True)
from scipy.special import ellipe,ellipk
from boris import *

m = m_pr
q = e
i = 10 #tok v ovoju

a = 0.1 # ovoj

v0 = np.array([0.0,-3,1.9])
x0 = np.array([.05, 0.,0.02])
E = lambda x: np.array([0.,0.,0.])
B = lambda x: B_bottle(x, a, a/2, i)
dt = 1e-5
tdur = 1e0
#print(tdur/dt)

X,V = boris(x0, v0, E, B, dt, tdur, q, m)
plot3(X, lim=[-1.2*a,1.2*a],bottle=[a,a/2])
X,V = boris(x0, v0, E, B, dt, tdur/10, q, m)
plot3(X)
