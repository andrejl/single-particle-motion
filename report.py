#!/usr/bin/python3 

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'family':'serif','serif':['Computer Modern']})
rc('text', usetex=True)

from boris import *

B = lambda x: np.array([0.,0.,x[0]**2+x[1]**2])
E = lambda x: np.array([0.,0.,0.])

x0 = np.array([0.,-1.,0.])
v0 = np.array([-0.2,2.5,0.])

X,V = boris(x0,v0,E,B,1e-2, 100)
plot3(X)
rplot(X)

B = lambda x: np.array([0.,0.,1.])
E = lambda x: np.array([-0.,0.8,0.])
x0 = np.array([0.,0.,0.])
v0 = np.array([0.,2.5,0.])
X,V = boris(x0,v0,E,B,1e-2, 20)

plot3(X)
rplot(X,fname='statika.pdf')
